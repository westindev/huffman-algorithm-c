# HUFFMAN COMPRESSEUR DECOMPRESSEUR 

Projet de C - Algorithmique L2B - 2021 - Université Paris VIII

> Membres : Axel LABARRE, OUKHEMANOU Mohand-Tahar, VILFEU Vincent

## OBJECTIF

Le but est de creer un programme en C appele huff qui compresse/decompresse
des fichiers selon le codage de Huffman. 
La compression/décompression doit utiliser le codage de Huffman (sans perte
donc) avec le code de huffman utilisé pour la compression inclus dans le fichier compressé .huff qui sera écrit en en-tête du fichier par exemple.

##  ORGANISATION / UTILISATION 


Le programme huff s’utilise de la façcon
suivante : 

```zsh
gcc huff.c
```
qui va générer notre programme compression/decompression a.out


```zsh
 ./a.out -c fichier.txt
 ```
qui construit le fichier.txt.**huff** compressé selon le
code de Huffman.

```zsh
./a.out -d fichier.txt.huff 
```
qui décompresse le fichier préalablement compressé en un fichier 

```zsh
./fichier.txt.huff.decomp
```

## PROGRESSION
---
![110%](https://progress-bar.dev/110/)



### ETAPES
---

### **COMPRESSION**
---
* 1. Calcul des occurences de chaque caractère présent dans le fichier source et génération d'un tableau d'occurrences. 

* 2. Initialisation d'un tableau d'arbres plus précisément de pointeurs de noeuds arbres constitués des propriétés du codage de huffman pour chaque caractères dont l'occur > 0.

* 3. Création de la fonction de tri pour trié le tableau des arbres dans un premier temps dans l'ordre croissant. 

* 4. Création de l'arbre de huffman en traitant les plus petites occurrences en premier grâce au tri précédent, et à l'aide d'une boucle while, fusionner les 2 premiers noeuds dans un nouveau noeud parent qui écrasera le 1er noeud du tableau et se placera a l'index 0 tandis que le second sera décalé tout à la fin du tableau puis vider à NULL,et enfin retrier le tableau pour que la fusion des occurrences des 2 premiers noeuds soient reclassé et ce jusqu'au dernier noeud qui contiendra la somme de toutes les occurences des caractères du fichier et deviendra la racine de l'arbre de huffman. 

* 5. Création du code binaire et du nombres de bits en fonction du niveau des feuilles de l'arbre via un parcours préfixe. 

* 6. Ecriture de l'en-tête permettant de reconstruire l'arbre de huffman lors de la décompression (nb de symboles différents "feuilles", symbole, etc ).

* 7. Encodage, ecriture du code compréssé des caractères du fichier + statistiques de compression. 

---
### **DECOMPRESSION**
---
* 8. Refaire le travail inverse pour recréer l'arbre de huffman en décodant les bits du fichier compréssé en commençant par lire son en-tête, reconstruire l'arbre puis lire l'encodage.